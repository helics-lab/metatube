- **1.0.7** (2022-03-02):
    - fixed errors in documentation

- **1.0.4** (2021-01-11):
    - refactoring
    - improved README
    - different hash length

- **1.0.3** (2021-01-08):
    - migrated from psycopg2 to SQLAlchemy (ORM)
    - build process now based on pep517
    - by default, records are now pseudonymised

- **1.0.2** (2020-12-04):
    - removed Arch-only AUR PKGBUILD

- **1.0.1** (2020-09-07):
    - fixing bugs in last night’s new features

- **1.0.0** (2020-09-07):
    - feature: better download history management, 
      can now catch up forwards, backwards, and gaps

- **0.1.1** (2020-04-30):
    - bugfix: datetime processing

- **0.1.0** (2020-04-29):
    - removed left-over debug statements

- **0.0.5** (2020-04-28):
    - many:
        - many:
            - typos in README, CHANGELOG, etc.

- **0.0.4** (2020-04-28):
    - release on:
        - PyPi
        - AUR
        - Zenodo

- **0.0.3** (2020-04-28):
    - fixes:
        - better paging of results (time-based), this is persistent over
          quota-resets
        - prevent API from returning one single item over and over again 
          (now able to break out of loop and exit after work is finished)

- **0.0.2** (2020-03-20): bugfixes

- **0.0.1** (2020-03-19): First functional release
